(async function f() {
    const {Aki} = require('aki-api');
    const chalk = require('chalk');
    const aki = new Aki('fr');
    const startingMessage = chalk.yellow('[AKINATOR] pense à quelqu\'un et je devinerai');
    const { Select } = require('enquirer');

    // message d'entrée
    console.log(startingMessage);

    await aki.start();

    while (aki.progress < 70  || aki.currentStep >= 78  )
    {
        //je pose une question
        console.log('question: ', aki.question);

        //je prépare des réponses
        const prompt = new Select({
            name: 'color',
            message: 'choisi une réponse',
            choices: ['oui', 'non', 'je ne sais pas', 'probablement', 'probablement pas']
        });

        // je propose les réponses
         const reponseString = await prompt.run()
            // quand il a répondu
            .catch(console.error);
    let reponse = 0;

    // me permet de récupérer l'index de la réponse
   if (reponseString == "non"){  reponse = 1;}
   else if (reponseString == "je ne sais pas"){  reponse = 2;}
   else if (reponseString == "probablement"){  reponse = 3;}
   else if (reponseString == "probablement pas"){  reponse = 4;}

    // je donne la réponse à akinator
    await aki.step(reponse);
    }

    // conditions de victoire
    if (aki.progress >= 70 || aki.currentStep >= 78) {
        await aki.win();
        console.log('c\'est : ' + aki.answers[0].name );

        // nombre de tour
        // console.log('guessCount:', aki.guessCount);
    }
    // probabilité de bonne réponse
    // console.log("aki-progresse" + aki.progress);

})();
